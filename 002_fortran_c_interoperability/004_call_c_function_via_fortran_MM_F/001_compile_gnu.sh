#!/bin/bash

# 1. Fortran object

  gfortran  -c                 \
            -O3                \
            -Wall              \
            -std=f2008         \
            m_1_parameters.f90

# 2. C object

  gcc  -c              \
       -Wall           \
       -O3             \
       -std=c99        \
       m_2_c_routine.c

# 3. Fortran main program
  
  gfortran  -O3                      \
            -Wall                    \
            -std=f2008               \
            m_1_parameters.o         \
            m_2_c_routine.o          \
            m_3_pass_string_to_c.f90 \
            -o x_gnu

# 4.

  rm *.o
  rm *.mod

