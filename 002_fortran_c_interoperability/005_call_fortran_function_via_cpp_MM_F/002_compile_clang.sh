#!/bin/bash

# 1 --> Fortran object

  gfortran  -c                      \
            -O3                     \
            -Wall                   \
            -std=f2008              \
	        -fopenmp                \
            m_1_fortran_vec_add.f90

# 2 --> C++ main program
  
  clang++  -O3                   \
           -Wall                 \
           -ansi                 \
           -fopenmp              \
           m_1_fortran_vec_add.o \
           driver_program.cpp    \
           -o x_clang

# 3 --> clean up

  rm *.o

