#!/bin/bash

# 1. Fortran object

  gfortran  -c                     \
            -O3                    \
            -Wall                  \
            -std=f2008             \
	        -fopenmp               \
            m_1_fortran_matmul.f90

# 3. C++ main program
  
  g++  -O3                   \
       -Wall                 \
       -ansi                 \
       m_1_fortran_matmul.o  \
       driver_program.cpp    \
       -L/usr/lib -lgfortran \
       -fopenmp              \
       -o x_gnu

# 4.

  rm *.o
