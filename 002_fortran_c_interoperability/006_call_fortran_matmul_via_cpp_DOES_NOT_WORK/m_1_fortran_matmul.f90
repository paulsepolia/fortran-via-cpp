!===============================!
! Author: Pavlos G. Galiatsatos !
! Date: 2013/07/11              !
!===============================!

  subroutine fortran_matmul(mat_a, &
                            mat_b, &
                            mat_c, &
                            rows,  &
                            columns)

  use iso_c_binding, only: C_DOUBLE, C_INT

  implicit none

  integer(kind=C_INT), intent(in)                                     :: rows
  integer(kind=C_INT), intent(in)                                     :: columns
  real(kind=C_DOUBLE), dimension(0:rows-1,0:columns-1), intent(inout) :: mat_a
  real(kind=C_DOUBLE), dimension(0:rows-1,0:columns-1), intent(inout) :: mat_b
  real(kind=C_DOUBLE), dimension(0:rows-1,0:columns-1), intent(inout) :: mat_c

  mat_c = matmul(mat_a, mat_b)

  write(*,*) mat_a(2,1)
  write(*,*) mat_c(2,2)

  end subroutine fortran_matmul
