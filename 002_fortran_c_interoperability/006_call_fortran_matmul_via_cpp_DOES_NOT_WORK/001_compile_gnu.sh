#!/bin/bash

# 1. Fortran object

  gfortran  -c                     \
            -O3                    \
            -Wall                  \
            -std=f2008             \
            m_1_fortran_matmul.f90

# 3. C++ main program
  
  g++  -O3                         \
       -Wall                       \
       -ansi                       \
       m_1_fortran_matmul.o        \
       m_3_fortran_matmul_main.cpp \
       -L/usr/lib -lgfortran       \
       -o x_gnu

# 4.

  rm *.o
