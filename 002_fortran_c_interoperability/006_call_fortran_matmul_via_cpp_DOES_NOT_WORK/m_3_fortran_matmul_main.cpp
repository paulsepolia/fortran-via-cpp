//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2013/07/11              //
//===============================//

#include "m_2_fortran_matmul_cpp.h"
#include <iostream>
#include <cmath>

using namespace std;

int main()
{
	// 1 --> variables

  	const int rows = 100;
	const int columns = 100;
 
	double ** mat_a = new double * [columns];
  	double ** mat_b = new double * [columns];
  	double ** mat_c = new double * [columns];
  
  	// 2 --> allocation of RAM
 
  	for (int i = 0; i != columns; ++i)
  	{
    		mat_a[i] = new double [rows];
    		mat_b[i] = new double [rows];
    		mat_c[i] = new double [rows];
  	}

  	// 3 --> initialization

  	for(int j = 0; j != rows; ++j)
  	{
    		for(int i = 0; i < columns; ++i)
    		{
      			mat_a[i][j] = static_cast<double>(i+j);
      			mat_b[i][j] = static_cast<double>(i+j);
      			mat_c[i][j] = static_cast<double>(1.0);
    		}
  	}

  	// 4 --> call the fortran defined function

  	cout << " ---> 1 " << endl;

  	fortran_matmul_(mat_a, mat_b, mat_c, &rows, &columns);

  	cout << " ---> 2 " << endl;

  	cout << " mat_c[1][2] --> " << mat_c[1][2] << endl;

  	// 5 --> deallocation
  
  	// a

  	for (int i = 0; i != columns; ++i)
  	{
    		delete [] mat_a[i];
    		delete [] mat_b[i];
    		delete [] mat_c[i];
  	}

  	// b
   
  	delete [] mat_a;
  	delete [] mat_b;
  	delete [] mat_c;

	// sentineling

	int sentinel;
	cin >> sentinel;

  	return 0;
}
