#!/bin/bash

# 1.

  gfortran  -c                  \
            -O3                 \
            -Wall               \
            -std=f2008          \
            m_1_pid_printer.f90

# 2.

  gfortran  -O3               \
            -Wall             \
            -std=f2008        \
            -static           \
            m_1_pid_printer.o \
            -o x_gnu

# 3.

  rm *.o
