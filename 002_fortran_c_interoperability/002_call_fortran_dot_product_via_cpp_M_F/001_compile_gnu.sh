#!/bin/bash

# 1 --> Fortran object

  gfortran  -c                  \
            -O3                 \
            -Wall               \
            -std=f2008          \
            m_1_fortran_dot.f90

# 2 --> C++ main program
  
  g++  -O3                   \
       -Wall                 \
       -ansi                 \
       -fopenmp              \
       m_1_fortran_dot.o     \
       driver_program.cpp    \
       -o x_gnu

# 3 --> clean up

  rm *.o

