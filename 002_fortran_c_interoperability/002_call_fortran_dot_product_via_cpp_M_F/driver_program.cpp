//===============================//
// Author: Pavlos G. Galiatsatos //
// Date: 2014/10/21              //
//===============================//

#include <iostream>
#include <cmath>
#include <iomanip>
#include <ctime>
#include <omp.h>

#include "m_2_fortran_dot_cpp.h"

using std::endl;
using std::cin;
using std::cout;
using std::pow;
using std::fixed;
using std::setprecision;
using std::clock;

// the main function

int main()
{
	// --> local parameters

  	const long DIM_VEC = 1 * static_cast<long>(pow(10.0, 8.0));
	const long K_MAX = static_cast<long>(pow(10.0, 4.0));

	// --> local variables	

  	double res_dot;
	long i;
	long k;
	clock_t t1;
	clock_t t2;
	int nt;

	// --> main test for-loop

	for (k = 0 ; k != K_MAX; ++k)
	{
		// --> counter

		cout << "------------------------------------------------------>> " << k << endl;
 
  		// --> allocation of RAM

		cout << " --> RAM allocation" << endl;

  		double * array_a = new double [DIM_VEC];
  		double * array_b = new double [DIM_VEC];

		// --> adjust the output format

		cout << fixed;
		cout << setprecision(20);

  		// --> initialization

		cout << " --> building the arrays" << endl;

		t1 = clock();

		#pragma omp parallel default(none)   \
				     shared(array_a) \
				     shared(array_b) \
			     	     private(i)
		{
			#pragma omp for

  			for(i = 0; i < DIM_VEC; ++i)
  			{
    				array_a[i] = sin(static_cast<double>(i));
    				array_b[i] = cos(static_cast<double>(i));
  			}
		}

		// get the number of openmp threads

		#pragma omp parallel default(none) \
				     shared(nt)
		{
			nt = omp_get_num_threads();
		}

		t2 = clock();

		cout << " --> time used = " 
		     << (t2-t1+0.0)/(CLOCKS_PER_SEC * nt) << endl;

  		// --> call the fortran defined function

		cout << " --> fortran_dot execution via cpp" << endl;

		t1 = clock();

  		fortran_dot_(array_a, array_b, &res_dot, &DIM_VEC);

		t2 = clock();

  		cout << " --> res_dot = " << res_dot << endl;
		cout << " --> time used = " << (t2-t1+0.0)/CLOCKS_PER_SEC/nt << endl;

  		// --> free up RAM

		cout << " --> delete RAM" << endl;

  		delete [] array_a;
  		delete [] array_b;
 
	}
 
	// XX --> sentineling

	cout << " --> end" << endl;

	int sentinel;
	cin >> sentinel;

  	return 0;
}

//======//
// FINI //
//======//

