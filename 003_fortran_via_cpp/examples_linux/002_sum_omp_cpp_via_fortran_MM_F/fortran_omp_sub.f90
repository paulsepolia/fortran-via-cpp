  
  subroutine fortran_omp_sub( resta, imax1, imax2, nt )
    
  implicit none  
   
  ! 1. declarations of variables 

  integer*4, intent(in)  :: imax1, imax2, nt
  real*8,    intent(out) :: resta
  integer*8              :: i, j

  ! 2. openmp part
    
  !$OMP PARALLEL           &
  !$OMP DEFAULT(NONE)      &
  !$OMP PRIVATE(i)         &
  !$OMP PRIVATE(j)         &
  !$OMP SHARED(imax1)      &
  !$OMP SHARED(imax2)      &
  !$OMP NUM_THREADS(NT)    &
  !$OMP REDUCTION(+:RESTA) 
    
  !$OMP DO

  do i = 1, 10 ** imax1
    do j = 1, 10 ** imax2
    
      resta = resta + (real(i,kind=8) + real(j,kind=8))  
      
    end do
  end do     
    
  !$OMP END DO
  !$OMP END PARALLEL
    
  end subroutine fortran_omp_sub

