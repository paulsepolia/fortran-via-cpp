  
  module mod1
  
  contains
  
  subroutine fortran_omp_sub_mod( resta, imax1, imax2, nt )
    
  implicit none  
   
  ! 1. declarations of variables 

  integer*4, intent(in)  :: imax1, imax2, nt
  real*8,    intent(out) :: resta
  integer*4              :: i, j

  ! 2. openmp part.
    
  !$OMP PARALLEL DO        &
  !$OMP DEFAULT(NONE)      &
  !$OMP PRIVATE(i)         &
  !$OMP PRIVATE(j)         &
  !$OMP SHARED(imax1)      &
  !$OMP SHARED(imax2)      &
  !$OMP NUM_THREADS(NT)    &
  !$OMP REDUCTION(+:RESTA) 
    
  do i = 1, 10 ** imax1
    do j = 1, 10 ** imax2
    
      resta = resta + ( i + j )  
      
    end Do
  end Do     
    
  !$OMP END PARALLEL DO
    
  end subroutine fortran_omp_sub_mod

  end module mod1