
 program driver_program 
 
 use mod1
 
 implicit none 
 
 real*8 :: resta
 integer*4 :: imax1, imax2, nt 
 
 imax1 = 6 
 imax2 = 5
 nt = 2 
 
 call fortran_omp_sub( resta, imax1, imax2, nt )
 
 write(*,*) " rest = ", resta 
 
 end program driver_program