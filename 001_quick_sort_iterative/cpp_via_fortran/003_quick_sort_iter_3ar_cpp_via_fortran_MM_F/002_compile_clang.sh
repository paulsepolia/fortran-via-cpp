#!/bin/bash

# 1. compile fortran

  gfortran  -c                      \
	        -O3                     \
            -std=f2008              \
            -Wall                   \
            quick_sort_iter_3ar.f90

# 2. compile c++

  clang++  -c                 \
           -O3                \
           driver_program.cpp

# 3. link c++ and fortran objects

  clang++  -O3                   \
           quick_sort_iter_3ar.o \
           driver_program.o      \
           -lgfortran            \
           -o x_clang

# 4. clean

  rm *.o
